package fr.d2craft.item;

import net.minecraft.util.IStringSerializable;

/**
 * Created by Mr. Dj on 09/04/2017.
 */
public enum EnumFlawedGem implements IStringSerializable {

    AMETHYST(0, "amethyst"),
    DIAMOND(1, "diamond"),
    EMERALD(2, "emerald"),
    RUBY(3, "ruby"),
    SAPPHIRE(4, "sapphire"),
    SKULL(5, "skull");

    private final int meta;
    private final String name;

    private EnumFlawedGem(int meta, String name)
    {
        this.meta = meta;
        this.name = name;
    }

    public int getMetadata()
    {
        return this.meta;
    }

    public String getName()
    {
        return this.name;
    }

    public static String getUnlocalizedName(int meta) {
        int i = 0;
        for (EnumFlawedGem enumflawedgem : values()) {
            if (i == meta) {
                return enumflawedgem.getName();
            }
            i++;
        }
        return "amethyst";
    }
}

package fr.d2craft.item;

import fr.d2craft.D2craft;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSword;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Mr. Dj on 01/04/2017.
 */
public class ModItems {

    public static Item trashItem;
    public static ItemSword masterPieceSword;

    public static void preInit() {
        trashItem = new ItemTrashItem("trash_item");
        registerItems();
    }

    public static void registerItems() {
        GameRegistry.register(trashItem, new ResourceLocation(D2craft.MODID, "trash_item"));
    }

    public static void registerRenders() {
        registerRender(trashItem);
    }

    public static void registerRender(Item item) {
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(D2craft.MODID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
    }

}

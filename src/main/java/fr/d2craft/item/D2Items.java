package fr.d2craft.item;

import fr.d2craft.D2Craft;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Mr. Dj on 09/04/2017.
 */
public class D2Items {

    public static FlawedGem flawedGem;

    public static final void commonPreInit() {
        flawedGem = (FlawedGem) registerItem(new FlawedGem(), "flawed");
    }

    public static final void clientInit() {
        for(int i = 0; i < EnumFlawedGem.values().length; i++) {
            registerRender(flawedGem, i, "flawed_" + EnumFlawedGem.values()[i].getName());
        }
    }

    private static final Item registerItem(Item item, String name) {
        GameRegistry.register(item, new ResourceLocation(D2Craft.MODID, name));
        item.setUnlocalizedName(name);
        return item;
    }

    private static final void registerRender(Item item) {
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0 ,new ModelResourceLocation(D2Craft.MODID + ":" + item.getRegistryName().getResourcePath(), "inventory"));
    }

    private static final void registerRender(Item item, int meta, String fileName) {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(D2Craft.MODID + ":" + fileName, "inventory"));
    }

}

package fr.d2craft.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import static fr.d2craft.D2Craft.tabD2Craft;

/**
 * Created by Mr. Dj on 09/04/2017.
 */
public class FlawedGem extends Item {

    public FlawedGem() {
        this.setCreativeTab(tabD2Craft);
        this.maxStackSize = 1;
        this.setMaxDamage(0);
        this.setHasSubtypes(true);
    }

    public String getUnlocalizedName(ItemStack stack)
    {
        int i = stack.getMetadata();
        if (i < 0 || 6 < i) {
            i = 0;
        }
        return super.getUnlocalizedName() + "_" + EnumFlawedGem.getUnlocalizedName(i);
    }

    @SideOnly(Side.CLIENT)
    public void getSubItems(Item itemIn, CreativeTabs tab, NonNullList<ItemStack> subItems)
    {
        for (int i = 0; i < 6; ++i)
        {
            subItems.add(new ItemStack(itemIn, 1, i));
        }
    }
}

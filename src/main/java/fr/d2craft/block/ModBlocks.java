package fr.d2craft.block;

import fr.d2craft.D2craft;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Mr. Dj on 01/04/2017.
 */
public class ModBlocks {

    public static Block trashBlock;

    public static void preInit() {
        trashBlock = new BlockTrashBlock(Material.ROCK, "trash_block");
        registerBlocks();
    }

    public static void registerBlocks() {
        registerBlock(trashBlock, "trash_block");
    }

    public static void registerBlock(Block block, String name) {
        GameRegistry.register(block, new ResourceLocation(D2craft.MODID, name));
        GameRegistry.register(new ItemBlock(block), new ResourceLocation(D2craft.MODID, name));
    }

    public static void registerBlock(Block block, ItemBlock itemBlock) {
        GameRegistry.register(block);
        GameRegistry.register(new ItemBlock(block).setRegistryName(block.getRegistryName()));
    }

    public static void registerRenders() {
        registerRender(trashBlock);
    }

    public static void registerRender(Block block) {
        Item item = Item.getItemFromBlock(block);
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(D2craft.MODID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
    }
}

package fr.d2craft;

import fr.d2craft.proxy.ClientProxy;
import fr.d2craft.proxy.CommonProxy;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Mod(modid = D2Craft.MODID, version = D2Craft.MODVERSION)
public class D2Craft {
	public static final String MODID = "d2craft";
	public static final String MODNAME = "D2Craft";
	public static final String MODVERSION = "1.0";
	public static final String MCVERSION = "1.11.2";
	public static final String PROXY = "fr.d2craft.proxy";
	
	@Instance
	public static D2Craft instance;
	
	@SidedProxy(clientSide = PROXY+".ClientProxy", serverSide = PROXY+".ServerProxy", modId = MODID)
	public static CommonProxy proxy;

	@SideOnly(Side.CLIENT)
	public static CreativeTabs tabD2Craft = new TabD2Craft(CreativeTabs.getNextID(), "tab" + MODNAME);
	
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		proxy.preInit(event);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
		proxy.init(event);
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		proxy.postInit(event);
	}
}

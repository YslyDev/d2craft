package fr.d2craft.proxy;

import fr.d2craft.item.D2Items;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ClientProxy extends CommonProxy {

	public void preInit(FMLPreInitializationEvent event) {
		super.preInit(event);

		//Init les textures des metadatas
		loadFlawed();
	}

	public void init(FMLInitializationEvent event) {
		super.init(event);

		D2Items.clientInit();
	}

	public void postInit(FMLPostInitializationEvent event) {
		super.postInit(event);
	}

	private void loadFlawed() {
		ModelLoader.setCustomModelResourceLocation(D2Items.flawedGem, 0, new ModelResourceLocation("d2craft:flawed_amethyst"));
		ModelLoader.setCustomModelResourceLocation(D2Items.flawedGem, 1, new ModelResourceLocation("d2craft:flawed_diamond"));
		ModelLoader.setCustomModelResourceLocation(D2Items.flawedGem, 2, new ModelResourceLocation("d2craft:flawed_emerald"));
		ModelLoader.setCustomModelResourceLocation(D2Items.flawedGem, 3, new ModelResourceLocation("d2craft:flawed_ruby"));
		ModelLoader.setCustomModelResourceLocation(D2Items.flawedGem, 4, new ModelResourceLocation("d2craft:flawed_sapphire"));
		ModelLoader.setCustomModelResourceLocation(D2Items.flawedGem, 5, new ModelResourceLocation("d2craft:flawed_skull"));
	}
}

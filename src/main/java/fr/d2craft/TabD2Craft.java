package fr.d2craft;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class TabD2Craft extends CreativeTabs {

	public TabD2Craft(int tabID, String tabName) {
		super(tabID, tabName);
		this.setBackgroundImageName("item_search.png");
	}

	@Override
	public ItemStack getTabIconItem() {
		return new ItemStack(Items.BED);
	}

}
